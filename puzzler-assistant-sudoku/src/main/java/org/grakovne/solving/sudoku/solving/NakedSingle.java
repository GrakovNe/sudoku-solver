package org.grakovne.solving.sudoku.solving;

import org.grakovne.solving.sudoku.domain.Board;
import org.grakovne.solving.sudoku.domain.Cell;
import org.grakovne.solving.sudoku.service.board.BoardService;
import org.grakovne.solving.sudoku.service.board.CellService;
import org.grakovne.solving.sudoku.service.solving.SolutionSearchService;

import static org.grakovne.solving.sudoku.domain.CellState.SOLVED;

public class NakedSingle extends SolutionSearchService {

    public NakedSingle(CellService cellService, BoardService boardService) {
        super(cellService, boardService);
    }

    @Override
    protected void updateCell(Board board, Cell cell) {
        if (cell.getState() == SOLVED) {
            return;
        }

        boardService
                .findRow(board, cell).stream().map(Cell::getValue)
                .forEach(it -> cellService.removeOption(cell, it));

        boardService
                .findColumn(board, cell).stream().map(Cell::getValue)
                .forEach(it -> cellService.removeOption(cell, it));

        boardService
                .findRegion(board, cell).stream().map(Cell::getValue)
                .forEach(it -> cellService.removeOption(cell, it));

        if (cellService.hasDefinedOption(cell)) {
            cellService.defineValue(cell, cellService.findFirstOption(cell));
        }

    }
}
