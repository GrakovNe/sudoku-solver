package org.grakovne.solving.sudoku;

import org.grakovne.solving.sudoku.domain.Board;
import org.grakovne.solving.sudoku.domain.BoardGameType;
import org.grakovne.solving.sudoku.domain.Cell;
import org.grakovne.solving.sudoku.service.BoardGameSolvingService;
import org.grakovne.solving.sudoku.service.board.BoardService;
import org.grakovne.solving.sudoku.service.board.CellService;
import org.grakovne.solving.sudoku.service.solving.SolutionSearchService;

import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.grakovne.solving.sudoku.domain.CellState.UNSOLVED;
import static org.grakovne.solving.sudoku.utils.StreamUtils.distinctByKey;

public class SudokuSolvingService extends BoardGameSolvingService {

    public SudokuSolvingService(CellService cellService,
                                BoardService boardService,
                                List<SolutionSearchService> solutionSearchServices) {

        super(cellService, boardService, solutionSearchServices);
    }

    @Override
    public List<Board> findSolution(Board origin) {
        logger.info("Started finding the solution of " + findGame() + " game");
        var solution = findSolution(List.of(origin));

        logger.info("Finished finding the solution of " + findGame() + " game");
        return solution;
    }

    @Override
    public BoardGameType findGame() {
        return BoardGameType.SUDOKU;
    }

    public List<Board> findSolution(List<Board> origin) {
        List<Board> reduced = origin
                .stream()
                .map(i -> reduceOptions(solutionSearchServices, i))
                .collect(toList());

        var hasSolution = reduced
                .parallelStream()
                .anyMatch(boardService::isBoardSolved);

        if (hasSolution) {
            return reduced
                    .parallelStream()
                    .filter(boardService::isBoardSolved)
                    .filter(distinctByKey(board -> board.getCells().parallelStream().map(Cell::getValue).collect(toList())))
                    .collect(toList());
        }

        List<Board> optionalBoards = reduced
                .parallelStream()
                .map(this::spawnBoards)
                .flatMap(List::stream)
                .filter(distinctByKey(board -> board.getCells().parallelStream().map(Cell::getValue).collect(toList())))
                .filter(it -> !boardService.isOptionsExhausted(it))
                .filter(it -> !boardService.isBoardBroken(it))
                .collect(toList());

        return findSolution(optionalBoards);
    }

    private List<Board> spawnBoards(Board board) {
        logger.debug("spawning a board options for unresolved cells");

        return board
                .getCells()
                .parallelStream()
                .filter(cell -> cell.getState() == UNSOLVED)
                .map(cell -> spawnOptionalValues(board, cell))
                .flatMap(List::stream)
                .collect(toList());
    }

    private List<Board> spawnOptionalValues(Board board, Cell cell) {
        logger.debug("spawning a values options for unresolved cells");

        return cell
                .getOptions()
                .parallelStream()
                .map(option -> spawnSuggestedValue(cell, board, option))
                .collect(toList());
    }


    private Board spawnSuggestedValue(Cell cell, Board board, Integer suggestion) {
        Board forkBoard = boardService.forkEntity(board);
        Cell forkCell = boardService.findByPosition(forkBoard, cell.getPosition().getRow(), cell.getPosition().getColumn());
        cellService.defineValue(forkCell, suggestion);
        return forkBoard;
    }

    private Board reduceOptions(List<SolutionSearchService> services, Board board) {
        logger.debug("trying to reduce options");

        Board finalBoard = board;
        if (services.stream().noneMatch(it -> it.isAcceptable(finalBoard))) {
            logger.debug("options reduction has been completed");
            return board;
        }

        for (SolutionSearchService it : services) {
            board = it.accept(board);
        }

        return reduceOptions(services, board);
    }

}
