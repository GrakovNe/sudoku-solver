package org.grakovne.solving;

import org.grakovne.solving.sudoku.domain.Board;
import org.grakovne.solving.sudoku.domain.Cell;
import org.grakovne.solving.sudoku.domain.CellState;
import org.grakovne.solving.sudoku.form.BoardForm;
import org.grakovne.solving.sudoku.service.board.BoardService;
import org.grakovne.solving.sudoku.service.board.CellService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import static java.util.stream.IntStream.range;

public class BoardServiceTest extends SudokuTest {

    private final CellService cellService = new CellService();
    private final BoardService boardService = new BoardService(cellService);
    private final Integer boardSize = 9;
    private final Integer regionSize = 3;

    @Test
    public void shouldCreateBoardFromForm() {
        List<Integer> rawValues = List.of(
                0, 1, 3, 8, 0, 0, 4, 0, 5,
                0, 2, 4, 6, 0, 5, 0, 0, 0,
                0, 8, 7, 0, 0, 0, 9, 3, 0,
                4, 9, 0, 3, 0, 6, 0, 0, 0,
                0, 0, 1, 0, 0, 0, 5, 0, 0,
                0, 0, 0, 7, 0, 1, 0, 9, 3,
                0, 6, 9, 0, 0, 0, 7, 4, 0,
                0, 0, 0, 2, 0, 7, 6, 8, 0,
                1, 0, 2, 0, 0, 8, 3, 5, 0
        );

        BoardForm form = buildBoardForm(boardSize, regionSize, rawValues);

        Board board = boardService.create(form);

        Assertions.assertEquals(Math.pow(boardSize, 2), board.getCells().size());
        Assertions.assertEquals(boardSize, board.getSize());
        Assertions.assertEquals(regionSize, board.getRegionSize());

        range(0, rawValues.size()).forEach(i -> assertCell(rawValues.get(i), board.getCells().get(i)));
    }

    @Test
    public void shouldFindRow() {
        List<Integer> rawValues = List.of(
                0, 1, 3, 8, 0, 0, 4, 0, 5,
                0, 2, 4, 6, 0, 5, 0, 0, 0,
                0, 8, 7, 0, 0, 0, 9, 3, 0,
                4, 9, 0, 3, 0, 6, 0, 0, 0,
                0, 0, 1, 0, 0, 0, 5, 0, 0,
                0, 0, 0, 7, 0, 1, 0, 9, 3,
                0, 6, 9, 0, 0, 0, 7, 4, 0,
                0, 0, 0, 2, 0, 7, 6, 8, 0,
                1, 0, 2, 0, 0, 8, 3, 5, 0
        );

        Board board = boardService.create(buildBoardForm(boardSize, regionSize, rawValues));

        Integer rowNumber = new Random().nextInt(9);

        Cell cell = board.getCells().get(rowNumber * board.getSize());

        List<Cell> neighbors = boardService.findRow(board, cell);

        range(rowNumber * board.getSize(), board.getSize()).forEach(it -> assertCell(rawValues.get(it), neighbors.get(it)));
    }

    @Test
    public void shouldFindColumn() {
        List<Integer> rawValues = List.of(
                0, 1, 3, 8, 0, 0, 4, 0, 5,
                0, 2, 4, 6, 0, 5, 0, 0, 0,
                0, 8, 7, 0, 0, 0, 9, 3, 0,
                4, 9, 0, 3, 0, 6, 0, 0, 0,
                0, 0, 1, 0, 0, 0, 5, 0, 0,
                0, 0, 0, 7, 0, 1, 0, 9, 3,
                0, 6, 9, 0, 0, 0, 7, 4, 0,
                0, 0, 0, 2, 0, 7, 6, 8, 0,
                1, 0, 2, 0, 0, 8, 3, 5, 0
        );

        Board board = boardService.create(buildBoardForm(boardSize, regionSize, rawValues));

        int columnNumber = new Random().nextInt(9);

        Cell cell = board.getCells().get(columnNumber);

        List<Cell> neighbors = boardService.findColumn(board, cell);

        for (int i = 0; i < rawValues.size(); i++) {
            int offset = i * boardSize + columnNumber;

            if (offset < rawValues.size() && offset != cell.getPosition().getRow() * boardSize + cell.getPosition().getColumn()) {
                assertCell(rawValues.get(offset), neighbors.get(i));
            }
        }

    }

    private void assertCell(Integer rawValue, Cell cell) {
        if (0 == rawValue) {
            Assertions.assertNull(cell.getValue());
            Assertions.assertEquals(CellState.UNSOLVED, cell.getState());

            List<Integer> options = range(1, 10).boxed().collect(Collectors.toList());
            Assertions.assertIterableEquals(options, cell.getOptions());
            return;
        }

        Assertions.assertEquals(rawValue, cell.getValue());
        Assertions.assertEquals(CellState.SOLVED, cell.getState());
    }
}
