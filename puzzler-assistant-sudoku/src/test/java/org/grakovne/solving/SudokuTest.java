package org.grakovne.solving;

import org.apache.commons.collections4.ListUtils;
import org.grakovne.solving.sudoku.form.BoardForm;
import org.grakovne.solving.sudoku.form.CellForm;
import org.grakovne.solving.sudoku.service.board.BoardService;
import org.grakovne.solving.sudoku.service.board.CellService;

import java.util.ArrayList;
import java.util.List;

public class SudokuTest {

    protected final CellService cellService = new CellService();
    protected final BoardService boardService = new BoardService(cellService);
    protected final Integer boardSize = 9;
    protected final Integer regionSize = 3;

    protected BoardForm buildBoardForm(Integer boardSize, Integer regionSize, List<Integer> rawValues) {
        BoardForm form = new BoardForm();

        form.setBoardSize(boardSize);
        form.setRegionSize(regionSize);
        form.setValues(buildValues(form.getBoardSize(), rawValues));

        return form;
    }

    private List<CellForm> buildValues(Integer boardSize, List<Integer> rawValues) {
        checkFormSize(boardSize, rawValues);

        List<CellForm> result = new ArrayList<>(boardSize * boardSize);
        buildFormValues(boardSize, result, ListUtils.partition(rawValues, boardSize));

        return result;
    }

    private void checkFormSize(Integer boardSize, List<Integer> rawValues) {
        if (rawValues.size() != boardSize * boardSize) {
            // up to validation layer
            throw new IllegalArgumentException("unable to work with not sized raw values");
        }
    }

    private void buildFormValues(Integer boardSize, List<CellForm> result, List<List<Integer>> rows) {
        for (int i = 0; i < boardSize; i++) {
            for (int j = 0; j < boardSize; j++) {
                CellForm form = new CellForm();
                form.setColumn(j);
                form.setRow(i);

                Integer value = rows.get(i).get(j);
                if (0 != value) {
                    form.setValue(value);
                }

                result.add(form);
            }
        }
    }
}
