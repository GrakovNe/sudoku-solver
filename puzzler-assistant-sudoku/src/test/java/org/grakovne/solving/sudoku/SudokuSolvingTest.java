package org.grakovne.solving.sudoku;

import org.grakovne.solving.SudokuTest;
import org.grakovne.solving.sudoku.domain.Board;
import org.grakovne.solving.sudoku.domain.Cell;
import org.grakovne.solving.sudoku.service.BoardGameSolvingService;
import org.grakovne.solving.sudoku.service.solving.SolutionSearchService;
import org.grakovne.solving.sudoku.solving.NakedSingle;
import org.junit.jupiter.api.Assertions;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public abstract class SudokuSolvingTest extends SudokuTest {

    public abstract List<Integer> getRawField();

    public abstract List<Integer> getRawSolution();

    public void shouldSolve() {
        List<SolutionSearchService> solvingServices = new ArrayList<>();
        solvingServices.add(new NakedSingle(cellService, boardService));

        BoardGameSolvingService solvingService = new SudokuSolvingService(cellService, boardService, solvingServices);

        Board board = boardService.create(buildBoardForm(boardSize, regionSize, getRawField()));
        Board solution = boardService.create(buildBoardForm(boardSize, regionSize, getRawSolution()));

        Board solvedBoard = solvingService.findSolution(board).get(0);
        Assertions.assertIterableEquals(solution.getCells().stream().map(Cell::getValue).collect(Collectors.toList()), solvedBoard.getCells().stream().map(Cell::getValue).collect(Collectors.toList()));
    }
}
