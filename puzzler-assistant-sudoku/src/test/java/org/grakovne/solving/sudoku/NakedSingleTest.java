package org.grakovne.solving.sudoku;

import org.grakovne.solving.SudokuTest;
import org.grakovne.solving.sudoku.domain.Board;
import org.grakovne.solving.sudoku.domain.Cell;
import org.grakovne.solving.sudoku.solving.NakedSingle;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Collectors;

public class NakedSingleTest extends SudokuTest {

    @Test
    public void shouldSolve() {
        List<Integer> rawValues = List.of(
                0, 0, 0, 0, 0, 8, 3, 7, 6,
                6, 3, 8, 2, 9, 7, 1, 4, 0,
                7, 1, 4, 6, 0, 3, 8, 9, 2,
                2, 7, 3, 4, 8, 5, 0, 6, 1,
                8, 9, 1, 7, 2, 6, 5, 3, 4,
                4, 5, 0, 3, 0, 9, 2, 8, 7,
                5, 8, 2, 9, 6, 4, 0, 1, 3,
                1, 4, 7, 8, 3, 2, 6, 5, 9,
                0, 6, 9, 5, 7, 1, 4, 0, 8
        );

        List<Integer> rawSolution = List.of(
                9, 2, 5, 1, 4, 8, 3, 7, 6,
                6, 3, 8, 2, 9, 7, 1, 4, 5,
                7, 1, 4, 6, 5, 3, 8, 9, 2,
                2, 7, 3, 4, 8, 5, 9, 6, 1,
                8, 9, 1, 7, 2, 6, 5, 3, 4,
                4, 5, 6, 3, 1, 9, 2, 8, 7,
                5, 8, 2, 9, 6, 4, 7, 1, 3,
                1, 4, 7, 8, 3, 2, 6, 5, 9,
                3, 6, 9, 5, 7, 1, 4, 2, 8
        );

        Board board = boardService.create(buildBoardForm(boardSize, regionSize, rawValues));
        Board solution = boardService.create(buildBoardForm(boardSize, regionSize, rawSolution));

        NakedSingle nakedSingle = new NakedSingle(cellService, boardService);

        Boolean isAcceptable = nakedSingle.isAcceptable(board);

        Assertions.assertTrue(isAcceptable);

        Board solvedBoard = nakedSingle.accept(board);

        Assertions.assertIterableEquals(solution.getCells().stream().map(Cell::getValue).collect(Collectors.toList()), solvedBoard.getCells().stream().map(Cell::getValue).collect(Collectors.toList()));

    }
}
