#Puzzler solving assistant

##How Sudoku Solved

Once initial board has been build, we should try to solve it with some "pen-n-pencil" strategies.

For now, there're following "pen-n-pencil" strategies resolved: 
 
 * Naked Single: It takes all filled values in the same row, column and region tha cell and removes defined values from options. If the only one option retains, it's a solution for this cell
 
When all "pen-n-pencil" strategies passed by, but board not solved yet, we make fork of board:
 * Each cell defined with first available option
 * Initial board turns into a list of boards with "defined" cell from first point
 * Each board resolving separately in recursion
 * If we've the board which has solution - we just fold recursion
 * If we've the board whic has a conflict - we just drop this board from list
 
##Questions:

**Why not DFS?**
It's causes to so far tree, so we've technical JVM restrictions to stack size and fails with stack overflow

**Why not Algorithm-X**
We just wanna to solve *any* board, not corrects only

**Which "pen-n-pencil" strategies could be implemented yet?**
See: https://www.kristanix.com/sudokuepic/sudoku-solving-techniques.php

