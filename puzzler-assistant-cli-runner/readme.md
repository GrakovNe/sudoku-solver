#Puzzler Assistant Cli Runner

**Should be used when it's need to solve board game natively in command line**

##How to run
Usage: `java -jar [artifact_name].jar --game [game type] --boardSize [size of board] --regionSize [size of region] --values [List of values]`

examples: 

- `java -jar [artifact_name].jar --game SUDOKU --boardSize 9 --regionSize 3 --values 0,1,3,8,0,0,4,0,5,0,2,4,6,0,5,0,0,0,0,8,7,0,0,0,9,3,0,4,9,0,3,0,6,0,0,0,0,0,1,0,0,0,5,0,0,0,0,0,7,0,1,0,9,3,0,6,9,0,0,0,7,4,0,0,0,0,2,0,7,6,8,0,1,0,2,0,0,8,3,5,0`

- `java -jar [artifact_name].jar --game SUDOKU --boardSize 9 --regionSize 3 --values 0,0,2,0,0,0,0,4,1,0,0,0,0,8,2,0,7,0,0,0,0,0,4,0,0,0,9,2,0,0,0,7,9,3,0,0,0,1,0,0,0,0,0,8,0,0,0,6,8,1,0,0,0,4,1,0,0,0,9,0,0,0,0,0,6,0,4,3,0,0,0,0,8,5,0,0,0,0,4,0,0`

##Application input

When application called, it's need to provide:
 - Game type (SUDOKU implemented for now)
 - Board size (i.e. 9 for board 9x9)
 - Region size if any
 - Values. They're listed with "," separator by rows

##Application output

After application done, you may see all found solutions in command line. Each of them printed as a two-dimension matrix