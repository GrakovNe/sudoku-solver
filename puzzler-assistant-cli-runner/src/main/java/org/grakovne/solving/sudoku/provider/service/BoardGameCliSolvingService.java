package org.grakovne.solving.sudoku.provider.service;

import org.grakovne.solving.sudoku.domain.Board;
import org.grakovne.solving.sudoku.provider.io.converter.BoardFormConverter;
import org.grakovne.solving.sudoku.provider.io.forms.BoardGameSolvingForm;
import org.grakovne.solving.sudoku.provider.io.service.GameBoardSolutionPrintingService;
import org.grakovne.solving.sudoku.provider.provider.SudokuSolvingServiceProvider;
import org.grakovne.solving.sudoku.service.BoardGameSolvingService;
import org.grakovne.solving.sudoku.service.board.BoardService;
import org.grakovne.solving.sudoku.service.board.CellService;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Know how to solve board game by user-cli data.
 * Like a business service.
 */
public class BoardGameCliSolvingService {

    public void solve(BoardGameSolvingForm gameSolvingForm) {
        List<BoardGameSolvingService> solvingServices = provideSolvingServices();
        BoardGameSolvingService solvingService = findSolvingService(gameSolvingForm, solvingServices);

        List<Board> solution = solvingService.findSolution(provideBoardService().create(provideBoardFormConverter().convert(gameSolvingForm)));
        provideGameBoardSolutionPrintingService().printSolutions(solution);
    }

    private List<BoardGameSolvingService> provideSolvingServices() {
        return List
                .of(new SudokuSolvingServiceProvider())
                .stream()
                .map(SudokuSolvingServiceProvider::provideService)
                .collect(Collectors.toList());
    }

    private BoardFormConverter provideBoardFormConverter() {
        return new BoardFormConverter();
    }

    private BoardGameSolvingService findSolvingService(BoardGameSolvingForm form, List<BoardGameSolvingService> solvingServices) {
        return solvingServices
                .stream()
                .filter(it -> it.findGame() == form.getGameType())
                .findAny()
                .orElseThrow(() -> new IllegalArgumentException("Game Type is not defined"));
    }

    private BoardService provideBoardService() {
        return new BoardService(new CellService());
    }

    private GameBoardSolutionPrintingService provideGameBoardSolutionPrintingService() {
        return new GameBoardSolutionPrintingService();
    }
}
