package org.grakovne.solving.sudoku.provider.io.forms;

import org.grakovne.solving.sudoku.domain.BoardGameType;

import java.util.List;

public class BoardGameSolvingForm {

    private BoardGameType gameType;
    private Integer boardSize;
    private Integer regionSize;
    private List<Integer> values;

    public BoardGameType getGameType() {
        return gameType;
    }

    public BoardGameSolvingForm setGameType(BoardGameType gameType) {
        this.gameType = gameType;
        return this;
    }

    public Integer getBoardSize() {
        return boardSize;
    }

    public BoardGameSolvingForm setBoardSize(Integer boardSize) {
        this.boardSize = boardSize;
        return this;
    }

    public Integer getRegionSize() {
        return regionSize;
    }

    public BoardGameSolvingForm setRegionSize(Integer regionSize) {
        this.regionSize = regionSize;
        return this;
    }

    public List<Integer> getValues() {
        return values;
    }

    public BoardGameSolvingForm setValues(List<Integer> values) {
        this.values = values;
        return this;
    }
}
