package org.grakovne.solving.sudoku.provider.io.converter;

import org.grakovne.solving.sudoku.converter.BaseConverter;
import org.grakovne.solving.sudoku.form.BoardForm;
import org.grakovne.solving.sudoku.form.CellForm;
import org.grakovne.solving.sudoku.provider.io.forms.BoardGameSolvingForm;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Know how to make common board form from cli-specific board form.
 */
public class BoardFormConverter extends BaseConverter<BoardGameSolvingForm, BoardForm> {

    public BoardFormConverter() {
        super(BoardForm::new);
    }

    @Override
    protected BoardForm inflate(BoardGameSolvingForm incoming, BoardForm result) {
        result.setBoardSize(incoming.getBoardSize());
        result.setRegionSize(incoming.getRegionSize());
        result.setValues(inflateCells(incoming));

        return result;
    }

    private List<CellForm> inflateCells(BoardGameSolvingForm incoming) {
        return IntStream
                .range(0, incoming.getValues().size())
                .mapToObj(i -> inflateCell(incoming, i))
                .collect(Collectors.toList());
    }

    private CellForm inflateCell(BoardGameSolvingForm incoming, int i) {
        CellForm form = new CellForm();

        form.setValue(0 == incoming.getValues().get(i) ? null : incoming.getValues().get(i));
        form.setRow(i / incoming.getBoardSize());
        form.setColumn(i % incoming.getBoardSize());

        return form;
    }
}
