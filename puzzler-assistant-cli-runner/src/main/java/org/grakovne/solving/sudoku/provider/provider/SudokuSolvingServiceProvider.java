package org.grakovne.solving.sudoku.provider.provider;

import org.grakovne.solving.sudoku.SudokuSolvingService;
import org.grakovne.solving.sudoku.service.BoardGameSolvingService;
import org.grakovne.solving.sudoku.service.solving.SolutionSearchService;
import org.grakovne.solving.sudoku.solving.NakedSingle;

import java.util.List;

public class SudokuSolvingServiceProvider extends BoardGameSolvingServicesProvider {

    @Override
    public BoardGameSolvingService provideService() {
        return new SudokuSolvingService(provideCellService(), provideBoardService(), provideSolutionServices());
    }

    private List<SolutionSearchService> provideSolutionServices() {
        return List.of(new NakedSingle(provideCellService(), provideBoardService()));
    }
}
