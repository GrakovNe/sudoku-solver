package org.grakovne.solving.sudoku.provider.io.service;

import org.grakovne.solving.sudoku.domain.Board;

import java.util.List;

/**
 * Know how to print found solution.
 */
public class GameBoardSolutionPrintingService {

    public void printSolutions(List<Board> solutions) {
        System.out.println("Found: " + solutions.size() + " solutions");
        System.out.println();

        solutions.forEach(this::printSolution);
    }

    private void printSolution(Board board) {
        for (int j = 0; j < board.getSize(); j++) {
            for (int i = 0; i < board.getSize(); i++) {
                int offset = board.getSize() * j + i;
                System.out.print(board.getCells().get(offset).getValue() + " ");
            }
            System.out.println();
        }

        System.out.println();
    }
}
