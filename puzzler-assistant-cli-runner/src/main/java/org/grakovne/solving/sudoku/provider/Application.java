package org.grakovne.solving.sudoku.provider;

import org.grakovne.solving.sudoku.provider.io.BoardFormParser;
import org.grakovne.solving.sudoku.provider.io.forms.BoardGameSolvingForm;
import org.grakovne.solving.sudoku.provider.service.BoardGameCliSolvingService;

/**
 * Application entry point.
 */
public class Application {

    public static void main(String[] args) {
        BoardGameSolvingForm gameSolvingForm = new BoardFormParser().parseArguments(args);
        new BoardGameCliSolvingService().solve(gameSolvingForm);
    }


}
