package org.grakovne.solving.sudoku.provider.provider;

import org.grakovne.solving.sudoku.service.BoardGameSolvingService;
import org.grakovne.solving.sudoku.service.board.BoardService;
import org.grakovne.solving.sudoku.service.board.CellService;

/**
 * Knows how to provide a new one adjusted solving service.
 */
public abstract class BoardGameSolvingServicesProvider {

    public abstract BoardGameSolvingService provideService();

    protected BoardService provideBoardService() {
        return new BoardService(provideCellService());
    }

    protected CellService provideCellService() {
        return new CellService();
    }
}
