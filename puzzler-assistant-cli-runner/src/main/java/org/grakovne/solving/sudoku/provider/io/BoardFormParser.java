package org.grakovne.solving.sudoku.provider.io;

import org.apache.commons.lang3.tuple.Pair;
import org.grakovne.solving.sudoku.domain.BoardGameType;
import org.grakovne.solving.sudoku.provider.io.forms.BoardGameSolvingForm;
import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

import static org.grakovne.solving.sudoku.utils.LoggerUtils.createLogger;

/**
 * Know how to parse raw cli argument to cli-specific board form.
 */
public class BoardFormParser {

    private final Logger logger = createLogger();

    private final Map<String, BiFunction<String, BoardGameSolvingForm, BoardGameSolvingForm>> builders = Map.of(
            "--game", (s, form) -> form.setGameType(BoardGameType.valueOf(s)),
            "--boardSize", (s, form) -> form.setBoardSize(Integer.valueOf(s)),
            "--regionSize", (s, form) -> form.setRegionSize(Integer.valueOf(s)),
            "--values", (s, form) -> form.setValues(Arrays.stream(s.split(",")).map(Integer::valueOf).collect(Collectors.toList()))
    );

    public BoardGameSolvingForm parseArguments(String[] args) {
        BoardGameSolvingForm result = new BoardGameSolvingForm();
        var arguments = pairArguments(args);

        try {
            arguments.forEach(pair -> builders.get(pair.getLeft()).apply(pair.getRight(), result));
        } catch (RuntimeException exception) {
            logger.error("Unable to parse cli attributes due: " + exception.getMessage());
        }

        return result;
    }

    private List<Pair<String, String>> pairArguments(String[] args) {
        List<Pair<String, String>> arguments = new ArrayList<>();

        for (int i = 0; i < args.length - 1; i += 2) {
            arguments.add(Pair.of(args[i], args[i + 1]));
        }

        return arguments;
    }
}
