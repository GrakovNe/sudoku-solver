#Puzzler solving assistant

##Design of code change log
- Implemented a Sudoku solution in one single method
- Naked Single separated as a separate service
- Solving service's Board Fork switched from DFS to simple List of boards
- All **core** code has been abstracted and separated into `puzzler-assistant-core`

## Long story short

* Application may used as a solver of any board game, which may be P or NP task
* Each submodule (except runners) is a one more game solver which should be extends from `core`
* We *doesn't use* low-level abstraction like a matrix X*X of digits due:
    * It's too much hard to support this
    * it's too much hard to read this
    * It's not provide much better performance that Object-oriented way

##Questions:

*How should i implement a new one game solver?*
 - create a new one application module like a `puzzler-assistant-sudoku`
 - Solve and test your solution with `BoardGameSolvingService` implementations
 - Update runner to be able use a new one solver (other side you may implement Dependency injection and don't stuck on it)