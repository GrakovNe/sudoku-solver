package org.grakovne.solving.sudoku.utils;

import org.slf4j.Logger;

import static java.lang.StackWalker.Option.RETAIN_CLASS_REFERENCE;
import static java.lang.StackWalker.getInstance;
import static org.slf4j.LoggerFactory.getLogger;

public class LoggerUtils {

    public static Logger createLogger() {
        Class<?> callingClass = getInstance(RETAIN_CLASS_REFERENCE).getCallerClass();
        return getLogger(callingClass);
    }
}
