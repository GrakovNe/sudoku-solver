package org.grakovne.solving.sudoku.converter;

import java.util.function.Supplier;

public abstract class BaseConverter<From, To> {

    private final Supplier<To> blankTo;

    public BaseConverter(Supplier<To> blankTo) {
        this.blankTo = blankTo;
    }

    public To convert(From incoming) {
        if (null == incoming) {
            return null;
        }

        return inflate(incoming, blankTo.get());
    }

    protected abstract To inflate(From incoming, To result);
}
