package org.grakovne.solving.sudoku.service.board;

import org.grakovne.solving.sudoku.domain.Board;
import org.grakovne.solving.sudoku.domain.Cell;
import org.grakovne.solving.sudoku.form.BoardForm;
import org.grakovne.solving.sudoku.service.BaseEntityService;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;
import static org.grakovne.solving.sudoku.domain.CellState.SOLVED;

public class BoardService extends BaseEntityService<Board> {

    private final CellService cellService;

    public BoardService(CellService cellService) {
        this.cellService = cellService;
    }

    @Override
    public Board forkEntity(Board origin) {
        logger.debug("making a fork of board");
        Board fork = new Board();

        fork.setRegionSize(origin.getRegionSize());
        fork.setSize(origin.getSize());
        fork.setCells(forkCells(origin.getCells()));

        logger.debug("board fork creation has been completed");
        return fork;
    }

    public Board create(BoardForm form) {
        logger.debug("inflating a new one cell from form");
        Board result = new Board();

        result.setSize(form.getBoardSize());
        result.setRegionSize(form.getRegionSize());
        result.setCells(form.getValues().stream().map(cellForm -> cellService.create(findRowLength(form), cellForm)).collect(toList()));

        return result;
    }

    public Cell findByPosition(Board board, Integer row, Integer column) {
        return board.getCells().get(board.getSize() * row + column);
    }

    public List<Cell> findRow(Board board, Cell cell) {
        return findBy(board, it -> it.getPosition().getRow().equals(cell.getPosition().getRow()));
    }

    public List<Cell> findColumn(Board board, Cell cell) {
        return findBy(board, it -> it.getPosition().getColumn().equals(cell.getPosition().getColumn()));
    }

    public List<Cell> findRegion(Board board, Cell cell) {
        return findBy(board, it -> {
            int rowRegionNumber = cell.getPosition().getRow() / board.getRegionSize();
            int columnRegionNumber = cell.getPosition().getColumn() / board.getRegionSize();

            return (it.getPosition().getRow() / board.getRegionSize() == rowRegionNumber) &&
                    (it.getPosition().getColumn() / board.getRegionSize() == columnRegionNumber);
        });
    }

    public Boolean hasDifference(Board first, Board second) {
        return !extractValues(first).equals(extractValues(second));

    }

    public boolean isBoardBroken(Board origin) {
        for (Cell cell : origin.getCells()) {

            List<Cell> row = findRow(origin, cell).stream().filter(cellService.filterByState(SOLVED)).collect(toList());
            if (!cellService.isCellsUniques(row, SOLVED)) {
                return true;
            }

            List<Cell> column = findColumn(origin, cell).stream().filter(cellService.filterByState(SOLVED)).collect(toList());
            if (!cellService.isCellsUniques(column, SOLVED)) {
                return true;
            }

            List<Cell> region = findColumn(origin, cell).stream().filter(cellService.filterByState(SOLVED)).collect(toList());
            if (!cellService.isCellsUniques(region, SOLVED)) {
                return true;
            }
        }

        return false;
    }

    public boolean isBoardSolved(Board origin) {
        return origin.getCells().parallelStream().allMatch(cellService.filterByState(SOLVED));
    }

    public boolean isOptionsExhausted(Board board) {
        boolean isOptionsClosed = board.getCells().stream().map(Cell::getOptions).allMatch(it -> it.size() == 0);
        boolean isBoardSolved = board.getCells().stream().map(Cell::getState).allMatch(it -> it == SOLVED);

        return !isBoardSolved && isOptionsClosed;
    }

    private List<Integer> extractValues(Board first) {
        return first
                .getCells()
                .stream()
                .map(Cell::getValue)
                .collect(toList());
    }

    private List<Cell> findBy(Board board, Predicate<Cell> condition) {
        return board
                .getCells()
                .stream()
                .filter(condition)
                .collect(toList());
    }

    private List<Cell> forkCells(List<Cell> cells) {
        return cells
                .parallelStream()
                .map(cellService::forkEntity)
                .collect(Collectors.toList());
    }

    private Integer findRowLength(BoardForm form) {
        return form.getBoardSize();
    }
}
