package org.grakovne.solving.sudoku.domain;

import java.util.LinkedList;
import java.util.List;

public class Board {

    private Integer size;
    private Integer regionSize;

    private List<Cell> cells = new LinkedList<>();

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Integer getRegionSize() {
        return regionSize;
    }

    public void setRegionSize(Integer regionSize) {
        this.regionSize = regionSize;
    }

    public List<Cell> getCells() {
        return cells;
    }

    public void setCells(List<Cell> cells) {
        this.cells = cells;
    }
}
