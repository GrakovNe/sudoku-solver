package org.grakovne.solving.sudoku.service;

import org.slf4j.Logger;

import static org.grakovne.solving.sudoku.utils.LoggerUtils.createLogger;

public abstract class BaseEntityService<T> {

    protected final Logger logger = createLogger();

    public abstract T forkEntity(T origin);
}
