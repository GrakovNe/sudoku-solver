package org.grakovne.solving.sudoku.service.board;

import org.grakovne.solving.sudoku.domain.Cell;
import org.grakovne.solving.sudoku.domain.CellState;
import org.grakovne.solving.sudoku.domain.Position;
import org.grakovne.solving.sudoku.form.CellForm;
import org.grakovne.solving.sudoku.service.BaseEntityService;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;

import static java.util.stream.Collectors.toSet;
import static java.util.stream.IntStream.range;
import static org.grakovne.solving.sudoku.utils.StreamUtils.distinctByKey;

public class CellService extends BaseEntityService<Cell> {

    @Override
    public Cell forkEntity(Cell origin) {
        logger.debug("making a fork of cell");
        Cell fork = new Cell();

        fork.setValue(origin.getValue());
        fork.setState(origin.getState());
        fork.setOptions(forkOptions(origin));
        fork.setPosition(forkPosition(origin));

        logger.debug("cell fork creation has been completed");
        return fork;
    }

    public Cell create(Integer rowLength, CellForm form) {
        Cell result = new Cell();
        result.setPosition(createCellPosition(form.getRow(), form.getColumn()));
        createSellOptions(form.getValue(), rowLength, result);
        result.setValue(form.getValue());

        if (null != form.getValue()) {
            setSolvedState(result);
        }

        return result;
    }

    public Set<Integer> buildCompleteOptions(Integer limit) {
        return range(1, limit + 1)
                .boxed()
                .collect(toSet());
    }

    public boolean hasDefinedOption(Cell cell) {
        return cell.getOptions().size() == 1;
    }

    public Integer findFirstOption(Cell cell) {
        return cell.getOptions().iterator().next();
    }

    public void defineValue(Cell cell, Integer value) {
        cell.setValue(value);
        setSolvedState(cell);
    }

    public void setSolvedState(Cell cell) {
        setState(cell, CellState.SOLVED);
    }

    public void removeOption(Cell cell, Integer value) {
        if (hasOption(cell, value)) {
            cell.getOptions().remove(value);
        }
    }

    public Predicate<Cell> filterByState(CellState state) {
        return it -> it.getState() == state;
    }

    public boolean isCellsUniques(List<Cell> row, CellState state) {
        int size = row.size();
        return size == row.stream().filter(filterByState(state)).filter(distinctByKey(Cell::getValue)).count();
    }

    private boolean hasOption(Cell cell, Integer value) {
        return cell.getOptions().contains(value);
    }

    private void setState(Cell cell, CellState state) {
        cell.setState(state);
    }

    private HashSet<Integer> forkOptions(Cell origin) {
        return new HashSet<>(origin.getOptions());
    }

    private Position forkPosition(Cell origin) {
        Position forkPosition = new Position();
        forkPosition.setColumn(origin.getPosition().getColumn());
        forkPosition.setRow(origin.getPosition().getRow());
        return forkPosition;
    }

    private void createSellOptions(Integer value, Integer rowLength, Cell result) {
        result.setOptions(buildCompleteOptions(rowLength));

        if (null != value) {
            removeOption(result, value);
        }
    }

    private Position createCellPosition(Integer row, Integer column) {
        Position result = new Position();

        result.setRow(row);
        result.setColumn(column);

        return result;
    }
}
