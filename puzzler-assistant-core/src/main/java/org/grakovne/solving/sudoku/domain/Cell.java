package org.grakovne.solving.sudoku.domain;

import java.util.HashSet;
import java.util.Set;

public class Cell {

    private Position position;
    private Integer value;
    private CellState state = CellState.UNSOLVED;
    private Set<Integer> options = new HashSet<>();

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public CellState getState() {
        return state;
    }

    public void setState(CellState state) {
        this.state = state;
    }

    public Set<Integer> getOptions() {
        return options;
    }

    public void setOptions(Set<Integer> options) {
        this.options = options;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }
}
