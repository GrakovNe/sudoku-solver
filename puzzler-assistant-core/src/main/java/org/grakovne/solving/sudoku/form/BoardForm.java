package org.grakovne.solving.sudoku.form;

import java.util.List;

public class BoardForm {

    private Integer boardSize;
    private Integer regionSize;
    private List<CellForm> values;

    public Integer getBoardSize() {
        return boardSize;
    }

    public void setBoardSize(Integer boardSize) {
        this.boardSize = boardSize;
    }

    public Integer getRegionSize() {
        return regionSize;
    }

    public void setRegionSize(Integer regionSize) {
        this.regionSize = regionSize;
    }

    public List<CellForm> getValues() {
        return values;
    }

    public void setValues(List<CellForm> values) {
        this.values = values;
    }
}
