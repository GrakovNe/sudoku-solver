package org.grakovne.solving.sudoku.service;

import org.grakovne.solving.sudoku.domain.Board;
import org.grakovne.solving.sudoku.domain.BoardGameType;
import org.grakovne.solving.sudoku.service.board.BoardService;
import org.grakovne.solving.sudoku.service.board.CellService;
import org.grakovne.solving.sudoku.service.solving.SolutionSearchService;
import org.slf4j.Logger;

import java.util.List;

import static org.grakovne.solving.sudoku.utils.LoggerUtils.createLogger;

public abstract class BoardGameSolvingService {

    protected final Logger logger = createLogger();

    protected final CellService cellService;
    protected final BoardService boardService;
    protected final List<SolutionSearchService> solutionSearchServices;

    public BoardGameSolvingService(CellService cellService,
                                   BoardService boardService,
                                   List<SolutionSearchService> solutionSearchServices) {

        this.cellService = cellService;
        this.boardService = boardService;
        this.solutionSearchServices = solutionSearchServices;
    }

    public abstract List<Board> findSolution(Board origin);

    public abstract BoardGameType findGame();
}
