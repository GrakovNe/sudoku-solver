package org.grakovne.solving.sudoku.domain;

public enum CellState {
    SOLVED,
    UNSOLVED
}
