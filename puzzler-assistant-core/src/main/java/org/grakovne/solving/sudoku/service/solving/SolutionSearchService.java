package org.grakovne.solving.sudoku.service.solving;

import org.grakovne.solving.sudoku.domain.Board;
import org.grakovne.solving.sudoku.domain.Cell;
import org.grakovne.solving.sudoku.service.board.BoardService;
import org.grakovne.solving.sudoku.service.board.CellService;

public abstract class SolutionSearchService {

    protected final CellService cellService;
    protected final BoardService boardService;

    protected SolutionSearchService(CellService cellService, BoardService boardService) {
        this.cellService = cellService;
        this.boardService = boardService;
    }

    public Boolean isAcceptable(Board board) {
        return boardService.hasDifference(board, accept(board));
    }

    public Board accept(Board board) {
        Board forkBoard = boardService.forkEntity(board);
        forkBoard.getCells().forEach(it -> updateCell(forkBoard, it));
        return forkBoard;
    }

    protected abstract void updateCell(Board board, Cell cell);
}
