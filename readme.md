#Puzzler Assistant

**Simple application which could help with solving board games**

#How to Build
Application may built with all dependencies via Gradle:

`./gradlew shadowJar`

#How to Run

For application using, you should take one of implemented runners

For now, the only cli-runner is implemented. Guide how to run this you may find in puzzler-assistant-cli-runner/readme.md file